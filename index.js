module.exports = {
  ServerAdapterFastify: require('./classes/serverAdapter/Fastify'),
  RouterAdapterFastify: require('./classes/routeAdapter/Fastify'),
};
